﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace anagrami
{
    class mass
    {
        public String anagramm;
        public int hashCode;
        public int k;
    }
    class words
    {
        List<mass> hash = new List<mass>();
        int workHash(string str)
        {
            String st = "";
            List<char> h = new List<char>();
            foreach (char c in str)
                h.Add(c);
            h.Sort();
            foreach (char s in h)
                st = st + s;
            return (st.GetHashCode());
        }
        public void find(string[] NewFile)
        {
            foreach (String s in NewFile)
            {
                mass m = new mass();
                m.hashCode = workHash(s);
                m.anagramm = s;
                m.k = 0;
                if (this.hash.Find((mass hh) => hh.hashCode == m.hashCode) == null)
                {
                    this.hash.Add(m);
                }
                else 
                {
                    String str1 = this.hash.Find((mass hh) => hh.hashCode == m.hashCode).anagramm;
                    str1 = str1 + ' ' + s;
                    this.hash.Find((mass hh) => hh.hashCode == m.hashCode).anagramm = str1;
                    this.hash.Find((mass hh) => hh.hashCode == m.hashCode).k++;
                }
            }
            printAnagramm();
        }
        public void printAnagramm()
        {
            foreach (mass mm in hash)
            {
                if (mm.k > 0)
                    Console.WriteLine(mm.anagramm);
            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            words w1 = new words();
            String[] NewFile = File.ReadAllLines("test.txt", Encoding.UTF8);
            w1.find(NewFile);
        }
    }
}